#!/usr/bin/env python3
import sys
import yaml

if len(sys.argv) > 1:
    DOCKERCOMPOSE=sys.argv[1]
else:
    sys.exit("[e] Usage: "+sys.argv[0]+"[docker-compose.yml path]")

with open(DOCKERCOMPOSE,'r') as f:
    try:
        compose = yaml.safe_load(f)
        print(compose)
    except yaml.YAMLError as err:
        sys.exit(err)
    try:
        if (compose['services']['client']['cap_drop'][0] != 'ALL') or len(compose['services']['client']['cap_drop']) > 1:
                sys.exit('[e] client shoud drop ALL capabilities')
        if 'cap_add' in compose['services']['client']:
                sys.exit('[e] client has unnecessay capabilities')
        if compose['services']['server']['cap_drop'][0] != 'ALL' or len(compose['services']['server']['cap_drop']) > 1:
                sys.exit('[e] server has unnecessay capabilities')

        if len(compose['services']['server']['cap_add']) > 4:
                sys.exit('[e] server has unnecessay capabilities')
        if 'CHOWN' not in compose['services']['server']['cap_add']:
            if 'chown' not in compose['services']['server']['cap_add']:
                sys.exit('[e] server has unnecessay capabilities')
        if 'NET_BIND_SERVICE' not in compose['services']['server']['cap_add']:
            if 'net_bind_service' not in compose['services']['server']['cap_add']:
                sys.exit('[e] server has unnecessay capabilities')
        if 'SETUID' not in compose['services']['server']['cap_add']:
            if 'setuid' not in compose['services']['server']['cap_add']:
                sys.exit('[e] server has unnecessay capabilities')
        if 'SETGID' not in compose['services']['server']['cap_add']:
            if 'setgid' not in compose['services']['server']['cap_add']:
                sys.exit('[e] server has unnecessay capabilities')

    except KeyError as err:
        sys.exit('[e] client or server have unnecessary capabilities')
